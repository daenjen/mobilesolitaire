using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Card Stats")]
    public float newYOffset = 0.15f;
    public float newZOffset = 0.01f;

    [Header("Check Bools")]
    [HideInInspector]
    public bool canTimer = false;
    [HideInInspector]
    public bool canPlay = true;
    [HideInInspector]
    public bool firstClick = false;
    [HideInInspector]
    public bool canMove;

    [Header("GameObjects")]
    public GameObject CardObj;
    public GameObject DeckObj;
    public GameObject CardSlot;
    public GameObject[] MiddleCardPos;
    public GameObject[] UpperCardPos;

    public Card[] UpperCardStacks;

    [Header("Texts")]
    public Text TextScoreValue;
    public Text TextMoveValue;
    public Text TextTimerValue;

    [Header("Panels")]
    public GameObject PauseGamePanel;
    public GameObject WinGamePanel;

    [Header("Lists")]
    public List<string> myDeck;
    public List<string> discardPileCards = new List<string>();

    public List<string>[] middleCards;

    public List<string> trioCardsOnDisplay = new List<string>();
    public List<List<string>> trioDeck = new List<List<string>>();

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///LOCALS
    private int countFirstClick = 0;

    //Score
    private int maxScore = 0;
    private int maxMove = 0;

    private float timerCount = 0.0f;

    //Deck
    private int trioCards;
    private int trioCardsRemainder;
    private int deckLocation;

    //GameObjects
    private GameObject actualCard;

    //Seed string array
    public static string[] seeds = new string[] { "H", "D", "F", "S" };
    public static string[] values = new string[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

    //Lists
    private List<string> middleCard0 = new List<string>();
    private List<string> middleCard1 = new List<string>();
    private List<string> middleCard2 = new List<string>();
    private List<string> middleCard3 = new List<string>();
    private List<string> middleCard4 = new List<string>();
    private List<string> middleCard5 = new List<string>();
    private List<string> middleCard6 = new List<string>();

    //Vectors
    private Vector3 cardOldPosition;
    private Vector3 cardNewPosition;


    // Start is called before the first frame update
    void Start()
    {
        middleCards = new List<string>[] { middleCard0, middleCard1, middleCard2, middleCard3, middleCard4, middleCard5, middleCard6 };

        CardSlot = this.gameObject;

        PlayCards();
    }

    // Update is called once per frame
    void Update()
    {
        GetMouseClick();

        if (CheckWin())
        {
            WinGame();
        }

        UpdateTextInGame();

        #region CheckScore
        //Check min score value
        if (maxScore < 0)
        {
            maxScore = 0;
        }
        #endregion
        #region Timer
        //Start timer
        timerCount += Time.deltaTime;
        #endregion
    }

    public void PlayCards()
    {
        myDeck = CreateDeck();
        ShuffleDeck(myDeck);
        SortCardsOnTable();
        StartCoroutine(DealCardsOnTable());
        Draw3CardsMode();
    }

    IEnumerator DealCardsOnTable()
    {
        for (int i = 0; i < 7; i++)
        {
            float cardYOffset = 0;
            float cardZOffset = 0.03f;

            foreach (string card in middleCards[i])
            {
                yield return new WaitForSeconds(0.1f);

                //Instantiate cards
                GameObject newCard = Instantiate(CardObj, new Vector3(DeckObj.transform.position.x, DeckObj.transform.position.y, DeckObj.transform.position.z), Quaternion.Euler(new Vector3(0f, 0f, 0f)));
                //Card transition
                iTween.MoveTo(newCard, new Vector3(MiddleCardPos[i].transform.position.x, MiddleCardPos[i].transform.position.y - cardYOffset, MiddleCardPos[i].transform.position.z - cardZOffset), 0.5f);
                //Assign name
                newCard.name = card;
                //Assign row value
                newCard.GetComponent<Card>().row = i;
                //Flip cards at the bottom
                if (card == middleCards[i][middleCards[i].Count - 1])
                {
                    newCard.GetComponent<Card>().canFlipLeft = true;
                }
                //Add different offset for each card
                cardYOffset += newYOffset;
                cardZOffset += newZOffset;
                //Update discard pile
                discardPileCards.Add(card);
            }
        }

        //Remove the discarded cards to the deck list
        foreach (string card in discardPileCards)
        {
            if (myDeck.Contains(card))
            {
                myDeck.Remove(card);
            }
        }
        //Reset discard pile
        discardPileCards.Clear();
    }

    public static List<string> CreateDeck()
    {
        List<string> NewDeck = new List<string>();
        foreach (string s in seeds)
        {
            foreach (string v in values)
            {
                NewDeck.Add(s + v);
            }
        }
        return NewDeck;
    }

    void ShuffleDeck<T>(List<T> list)
    {
        System.Random random = new System.Random();
        int n = list.Count;

        //Shuffle cards
        while (n > 1)
        {
            int k = random.Next(n);
            n--;
            T temp = list[k];
            list[k] = list[n];
            list[n] = temp;
        }
    }

    void SortCardsOnTable()
    {
        for (int i = 0; i < 7; i++)
        {
            for (int j = i; j < 7; j++)
            {
                middleCards[j].Add(myDeck.Last<string>());

                //Fill each row after first loop
                myDeck.RemoveAt(myDeck.Count - 1);
            }
        }
    }

    public void Draw3CardsMode()
    {
        trioCards = myDeck.Count / 3;
        trioCardsRemainder = myDeck.Count % 3;
        trioDeck.Clear();

        int cardCount = 0;
        for (int i = 0; i < trioCards; i++)
        {
            //Temp list cards by 3 then add to list
            List<string> TrioCards = new List<string>();
            for (int j = 0; j < 3; j++)
            {
                TrioCards.Add(myDeck[j + cardCount]);
            }
            trioDeck.Add(TrioCards);
            cardCount += 3;
        }

        if (trioCardsRemainder != 0)
        {
            //Add to list the remaining cards
            List<string> TrioCardsRemainder = new List<string>();
            cardCount = 0;
            for (int k = 0; k < trioCardsRemainder; k++)
            {
                TrioCardsRemainder.Add(myDeck[myDeck.Count - trioCardsRemainder + cardCount]);
                cardCount++;
            }
            trioDeck.Add(TrioCardsRemainder);
            trioCards++;
        }

        //Reset deck location
        deckLocation = 0;
    }

    IEnumerator DealFromDeck()
    {
        //Count player move
        maxMove++;

        //Add remaining cards to discard pile
        foreach (Transform child in DeckObj.transform)
        {
            if (child.CompareTag("Card"))
            {
                myDeck.Remove(child.name);
                discardPileCards.Add(child.name);
                Destroy(child.gameObject);
            }
        }
        if (deckLocation < trioCards)
        {
            //Draw 3 new cards
            trioCardsOnDisplay.Clear();
            //Initial offset
            float xOffset = 1.25f;
            float zOffset = -0.2f;

            foreach (string card in trioDeck[deckLocation])
            {
                yield return new WaitForSeconds(0.1f);

                //Instantiate trio cards
                GameObject newTopCard = Instantiate(CardObj, new Vector3(DeckObj.transform.position.x, DeckObj.transform.position.y,
                                                    DeckObj.transform.position.z), Quaternion.identity, DeckObj.transform);
                //Card transition
                iTween.MoveTo(newTopCard, new Vector3(DeckObj.transform.position.x - xOffset, DeckObj.transform.position.y, DeckObj.transform.position.z + zOffset), 0.5f);

                //Set card values each loop
                xOffset -= 0.2f;
                zOffset -= 0.2f;
                newTopCard.name = card;
                trioCardsOnDisplay.Add(card);
                newTopCard.GetComponent<Card>().canFlipLeft = true;
                newTopCard.GetComponent<Card>().inDeckPile = true;

                //Check for UNDO function
                newTopCard.GetComponent<Card>().cardInDeck = true;
            }
            deckLocation++;
        }
        else
        {
            RestackTopDeck();
        }
    }

    public void RestackTopDeck()
    {
        //Reset deck
        myDeck.Clear();

        foreach (string card in discardPileCards)
        {
            myDeck.Add(card);
        }
        //Reset pile
        discardPileCards.Clear();
        Draw3CardsMode();

        //Remove score
        maxScore -= 100;
    }

    bool IsCardStackable(GameObject cardSelected)
    {
        //Temp ref
        Card CardSlotSelect = CardSlot.GetComponent<Card>();
        Card CardSelect = cardSelected.GetComponent<Card>();

        //Compare cards
        if (!CardSelect.inDeckPile)
        {
            if (CardSelect.upperZone) //if in upper zone
            {
                //Compare card seed
                if (CardSlotSelect.seed == CardSelect.seed || (CardSlotSelect.value == 1 && CardSelect.seed == null))
                {
                    //Check if card has value+1
                    if (CardSlotSelect.value == CardSelect.value + 1)
                    {
                        //Check if card has no children
                        if (CardSlotSelect.gameObject.transform.childCount == 0)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else //if in middle zone, check if opposite color
            {
                if (CardSlotSelect.value == CardSelect.value - 1)
                {
                    //Temp card1
                    bool card1 = true;
                    //Temp card2
                    bool card2 = true;

                    //If card in slot is black, is not red
                    if (CardSlotSelect.seed == "F" || CardSlotSelect.seed == "S")
                    {
                        card1 = false;
                    }

                    //If card select is black, is not red
                    if (CardSelect.seed == "F" || CardSelect.seed == "S")
                    {
                        card2 = false;
                    }

                    //If same color return false
                    if (card1 == card2)
                    {
                        return false;
                    }
                    else if(card1 != card2 && CardSlotSelect.upperZone == false)    //If opposite color and not top return true
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void StackCard(GameObject cardSelected)
    {
        //Check for UNDO 
        countFirstClick = 2;

        //Temp ref
        Card CardSlotSelect = CardSlot.GetComponent<Card>();
        Card CardSelect = cardSelected.GetComponent<Card>();
        float yOffset = 0.3f;

        //Check card Y offset in upper zone or middle zone
        if (CardSelect.upperZone || CardSelect.upperZone && CardSlotSelect.value == 13)
        {
            yOffset = 0;
        }
        else if (!CardSelect.upperZone && CardSlotSelect.value == 13)
        {
            yOffset = 0;
        }

        //Set card position
        CardSlot.transform.position = new Vector3(cardSelected.transform.position.x, cardSelected.transform.position.y - yOffset, cardSelected.transform.position.z - 0.01f);
        //Make it parent of the card slot
        CardSlot.transform.parent = cardSelected.transform;

        ///UNDO variables 
        //Get new position
        cardNewPosition = CardSlot.transform.position;
        CardSlotSelect.canUndo = true;
        actualCard = CardSlotSelect.gameObject;
        firstClick = false;

        //Remove card from upper pile to prevent duplicate cards
        if (CardSlotSelect.inDeckPile)   
        {
            trioCardsOnDisplay.Remove(CardSlot.name);
        }
        else if (CardSlotSelect.upperZone)    //keep track of current upper decks value after remove
        {
            UpperCardPos[CardSlotSelect.row].GetComponent<Card>().value = CardSlotSelect.value - 1;
        }
        else   //Remove card from middle zone
        {
            middleCards[CardSlotSelect.row].Remove(CardSlot.name);
        }

        //Remove check if in deck pile
        CardSlotSelect.inDeckPile = false;
        //Assign new row
        CardSlotSelect.row = CardSelect.row;

        //if card moves to the upper zone assign new value and seed
        if (CardSelect.upperZone) 
        {
            UpperCardPos[CardSlotSelect.row].GetComponent<Card>().value = CardSlotSelect.value;
            UpperCardPos[CardSlotSelect.row].GetComponent<Card>().seed = CardSlotSelect.seed;
            CardSlotSelect.upperZone = true;

            //Add score
            maxScore += 10;
            //Add player move
            maxMove++;
        }
        else
        {
            CardSlotSelect.upperZone = false;

            //Add score
            maxScore += 5;
            //Add player move
            maxMove++;
        }

        //Reset card slot
        CardSlot = this.gameObject;
    }

    public bool isCardBlocked(GameObject cardSelected)
    {
        Card cardSelect = cardSelected.GetComponent<Card>();

        //If the card is in the deck pile
        if (cardSelect.inDeckPile == true)
        {
            if (cardSelect.name == trioCardsOnDisplay.Last())    //If selected card is in the last trio list, do not block selection
            {
                return false;
            }
            else  //If selected card is not in the last trio list, block selection
            {
                return true;
            }
        }
        else  //If the card is not in the deck pile
        {
            if (cardSelect.name == middleCards[cardSelect.row].Last())   //If selected card is in the last row list, do not block selection
            {
                return false;
            }
            else  //If selected card is not in the last row list, block selection
            {
                return true;
            }
        }
    }

    void GetMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit)
            {
                if (hit.collider.CompareTag("Deck"))
                {
                    Deck();
                }
                else if (hit.collider.CompareTag("Card"))
                {
                    Card(hit.collider.gameObject);
                }
                else if (hit.collider.CompareTag("Top"))
                {
                    UpperZone(hit.collider.gameObject);
                }
                else if (hit.collider.CompareTag("Bottom"))
                {
                    MiddleZone(hit.collider.gameObject);
                }
            }
        }
    }

    void Deck()
    {
        StartCoroutine(DealFromDeck());
        CardSlot = this.gameObject;

        //UNDO variable
        canMove = false;
    }

    void Card(GameObject cardSelected)
    {
        print("Clicked on card");

        //Check for UNDO 
        countFirstClick = 1;

        #region Card Checks
        //Check if card clicked is face down, flip it
        if (!cardSelected.GetComponent<Card>().isFaceUp)
        {
            if (!isCardBlocked(cardSelected))
            {
                cardSelected.GetComponent<Card>().canFlipLeft = true;
                CardSlot = this.gameObject;
                canMove = false;
            }
        }
        else if (cardSelected.GetComponent<Card>().inDeckPile)  //if in the deck pile
        {
            if (!isCardBlocked(cardSelected))
            {
                //Select the card in deck
                if (CardSlot != cardSelected)
                {
                    CardSlot = cardSelected;
                }
            }
        }
        else
        {
            //Select the card
            if (CardSlot == this.gameObject)
            {
                CardSlot = cardSelected;
            }
            else if (CardSlot != cardSelected)
            {
                //check if selected card can stack to the old card
                if (IsCardStackable(cardSelected))
                {
                    StackCard(cardSelected);
                }
                else
                {
                    //select the new card
                    CardSlot = cardSelected;
                }
            }
        }
        #endregion

        //UNDO variables
        if (cardSelected.GetComponent<Card>().isFaceUp && countFirstClick == 1)
        {
            cardOldPosition = cardSelected.transform.position;
            cardNewPosition = Vector3.zero;
            firstClick = true;
            canMove = true;
        }
    }

    void UpperZone(GameObject cardSelected)
    {
        if (CardSlot.CompareTag("Card"))
        {
            //if card is ace and empty box, stack it
            if (CardSlot.GetComponent<Card>().value == 1 && CardSlot.GetComponent<Card>().seed == cardSelected.gameObject.GetComponent<Card>().seed)
            {
                StackCard(cardSelected);

                ///UNDO variables
                firstClick = false;
            }
        }
    }

    void MiddleZone(GameObject cardSelected)
    {
        if (CardSlot.CompareTag("Card"))
        {
            if (CardSlot.GetComponent<Card>().value == 13)
            {
                StackCard(cardSelected);
            }
        }
    }

    public void UndoMove()
    {
        if (cardOldPosition != null && actualCard != null && canMove == true)
        {
            if (cardNewPosition != Vector3.zero && countFirstClick == 2)
            {
                //Transition card
                iTween.MoveTo(actualCard, cardOldPosition, 0.5f);
                //Reset parent
                actualCard.GetComponent<Card>().gameObject.transform.parent = null;
                //Release UNDO check
                actualCard.GetComponent<Card>().canUndo = false;
                //Reset UNDO variables
                countFirstClick = 1;
                cardOldPosition = Vector3.zero;
                cardNewPosition = Vector3.zero;
                canMove = false;

                //Add player move
                maxMove++;

                //Check if card on upper zone
                if(actualCard.GetComponent<Card>().upperZone == true)
                {
                    actualCard.GetComponent<Card>().upperZone = false;
                }

                //If card was in deck, reset value
                if (actualCard.GetComponent<Card>().cardInDeck)
                {
                    actualCard.GetComponent<Card>().transform.parent = DeckObj.transform;
                    trioCardsOnDisplay.Add(actualCard.name);
                    actualCard.GetComponent<Card>().inDeckPile = true;
                    actualCard.GetComponent<Card>().row = 0;
                }
            }
        }
    }

    public bool CheckWin()
    {
        int i = 0;
        foreach (Card CardUpperZone in UpperCardStacks)
        {
            i += CardUpperZone.value;
        }
        if (i >= 52)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region UI/Buttons 
    public void UpdateTextInGame()
    {
        //Update text
        TextTimerValue.text = timerCount.ToString();
        TextScoreValue.text = maxScore.ToString();
        TextMoveValue.text = maxMove.ToString();
    }

    public void WinGame()
    {
        WinGamePanel.SetActive(true);

        Time.timeScale = 0;
    }

    public void RestartGame()   //When i press restart, reload scene MainMenu
    {       
        SceneManager.LoadScene("MainGame");
        Time.timeScale = 1;
    }

    public void OpenPauseMenu() //When i press Esc, pause the game and open PauseMenu
    {
        PauseGamePanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumePauseMenu()   //When i press resume, close the PauseMenu and unpause the game
    {
        PauseGamePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void BacktoMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1;
    }
    #endregion
}
