using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Card : MonoBehaviour
{
    [Header("CardStats")]
    public float timer = 0;
    public float x, y, z;
    public float degreesPerSec = 360f;

    [Header("CardSelection")]
    public bool upperZone = false;
    public string seed;
    public int value;
    public int row;
    
    [Header("Bools")]
    [HideInInspector]
    public bool inDeckPile = false;
    [HideInInspector]
    public bool cardInDeck = false;
    [HideInInspector]
    public bool isMainSeed = false;
    [HideInInspector]
    public bool isFaceUp = false;
    [HideInInspector]
    public bool isCardCover = false;
    [HideInInspector]
    public bool canFlipRight = false;
    [HideInInspector]
    public bool canFlipLeft = false;
    [HideInInspector]
    public bool canUndo = false;

    [Header("GameObjects")]
    public Sprite CardFace;
    public Sprite CardCover;

    public Sprite[] CardFaces;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ///LOCALS
    private string valueString;

    //GameObjects
    private GameManager MyManager;
    private SpriteRenderer SpriteRend;


    // Start is called before the first frame update
    void Start()
    {
        GetCardSprite();
        
        SpriteRend = GetComponent<SpriteRenderer>();

        if (isMainSeed==false)
        {
            CompareCard();
        }
    }

    // Update is called once per frame
    void Update()
    {
        FlipCardAnimation();

        CardColorOnSelection();
    }

    public void GetCardSprite()
    {
        List<string> deck = GameManager.CreateDeck();
        MyManager = FindObjectOfType<GameManager>();

        int i = 0;
        foreach (string card in deck)
        {
            if (this.name == card)
            {
                CardFace = CardFaces[i];
                break;
            }
            i++;
        }
    }

    public void FlipCardAnimation()
    {
        if (canFlipLeft == true)
        {
            FlipLeft();
        }
        else if (canFlipRight == true)
        {
            FlipRight();
        }
    }

    public void CardColorOnSelection()
    {
        if (MyManager.CardSlot)
        {
            if (name == MyManager.CardSlot.name)
            {
                //Gray color
                SpriteRend.color = new Color(0.8f, 0.8f, 0.8f);
            }
            else
            {
                SpriteRend.color = Color.white;
            }
        }
    }

    public void FlipLeft()
    {
        //Start timer
        timer += Time.deltaTime;

        float rotAmount = degreesPerSec * Time.deltaTime;
        float curRot = transform.localRotation.eulerAngles.y;
        transform.localRotation = Quaternion.Euler(new Vector3(0, curRot + rotAmount, 0));
        isCardCover = true;

        if (timer > 0.2f)
        {
            if (isCardCover)
            {
                SpriteRend.sprite = CardFace;
                SpriteRend.flipX = CardFace;
                isCardCover = false;
            }
        }

        if (timer >= 0.4f)
        {
            canFlipLeft = false;
            transform.eulerAngles = new Vector3(0f, 180f, 0f);
            timer = 0;
            isFaceUp = true;
        }
    }

    public void FlipRight()
    {
        //Start timer
        timer += Time.deltaTime;

        float rotAmount = degreesPerSec * -Time.deltaTime;
        float curRot = transform.localRotation.eulerAngles.y;
        transform.localRotation = Quaternion.Euler(new Vector3(0, curRot + rotAmount, 0));
        isCardCover = true;

        if (timer > 0.2f)
        {
            if (isCardCover)
            {
                SpriteRend.sprite = CardCover;
                SpriteRend.flipX = false;
                isCardCover = false;
            }
        }

        if (timer >= 0.4f)
        {
            canFlipRight = false;
            transform.eulerAngles = Vector3.zero;
            timer = 0;
            isFaceUp = false;
        }
    }

    private void CompareCard()
    {
        if (CompareTag("Card"))
        {
            //Get our seed card 
            seed = transform.name[0].ToString();

            //Get our value card
            for (int i = 1; i < transform.name.Length; i++)
            {
                char c = transform.name[i];
                valueString += c.ToString();
            }

            //Compare and assign value
            #region Compare and assign value
            if (valueString == "A")
            {
                value = 1;
            }
            if (valueString == "2")
            {
                value = 2;
            }
            if (valueString == "3")
            {
                value = 3;
            }
            if (valueString == "4")
            {
                value = 4;
            }
            if (valueString == "5")
            {
                value = 5;
            }
            if (valueString == "6")
            {
                value = 6;
            }
            if (valueString == "7")
            {
                value = 7;
            }
            if (valueString == "8")
            {
                value = 8;
            }
            if (valueString == "9")
            {
                value = 9;
            }
            if (valueString == "10")
            {
                value = 10;
            }
            if (valueString == "J")
            {
                value = 11;
            }
            if (valueString == "Q")
            {
                value = 12;
            }
            if (valueString == "K")
            {
                value = 13;
            }
            #endregion
        }
    }
}
